# SQR Lab 11

# Alexander Krivonosov SE-01

## VK

I decided to test [vk.com](http://vk.com), using the “Forgot password” scenario. Preliminary (expected) steps are following:

1. Go to the log in page of vk.com
2. Realise that I have forgotten my password
3. Try to reset the password for a non-existing account
4. Remember the result
5. Try to reset the password for an existing account
6. Remember the results 
7. Compare and conclude

So, let’s go step by step

### Non-existing account

When we go to the login page, we are prompted to enter an email of a phone number, there’s no way to say that we’ve forgotten it, so we enter the email that is not linked to any account. The for immediately tells us that this account does not exist:

![Untitled](SQR%20Lab%2011%20d4452594beda4bc891d712ae3e60b83e/Untitled.png)

So, we already can see that the test will fail - the result for existing and non-existing accounts are different because the app simply will not let you even start the password reset procedure for a non-existing account. But is this really a security issue?

### Existing account

When I inserted a valid number, it took me to the next page, prompting me to enter the last name that I specified for my account. After that, it required me to insert a code that I received via SMS. Only after that, I was allowed to reset my password.

### Conclusion

Although the test failed, I do not think that it poses any security threats, because the amount of effort that is required to bruteforce the email/phone number and the surname is huge and almost infeasible.

## Twitter

Next one is twitter. The expected steps are:

1. Go to the log in page of twitter.com
2. Realise that I have forgotten my password
3. Try to reset the password for a non-existing account
4. Remember the result
5. Try to reset the password for an existing account
6. Remember the results 
7. Compare and conclude

### Non-existing account

If we go to the login page and try to enter an email that is not linked to any account, we also get a message saying that this account does not exist:

![Untitled](SQR%20Lab%2011%20d4452594beda4bc891d712ae3e60b83e/Untitled%201.png)

So the situation is the same as with vk.com. But let’s check what happens if we insert a valid email.

### Existing account

When we enter a valid email, the website prompts us to enter the password. If we click “Forgot password?” button, it will start the procedure of restoring the password. First, it will ask us to enter the name of our account and our email again. After we do that, we see a message, saying that our request was taken into work and its review will probably take a few days.

### Conclusion

So we can see that although the results for existing and non-existing are very different, this does not pose any security threats, because resetting the password also requires you to know the name of the account.

## GitHub

The next one is github.com. Expected steps are the same, so let’s get to testing.

### Non-existing email

When we get to the login page we see the “Forgot password” button right away. We click on it and try to enter an email for non-existing account. This is the message that we get:

![Untitled](SQR%20Lab%2011%20d4452594beda4bc891d712ae3e60b83e/Untitled%202.png)

### Existing email

If we enter the email of an existing account, we receive a message, saying that they have sent a password reset link to our email. they didn’t require us to enter a username.

### Conclusion

So, GitHub also failed the test and that might be a security issue, because it does not require us to provide any additional information other that the email.

## Хабр

The last one is habr.com. Expected steps are the same.

### Non-existing email

When we get to the login page we see the “Forgot button” in place. Clicking on it will get us to the page where they ask us to enter the email. Entering the non-existing email will give a message, saying that such an account does not exist.

![Untitled](SQR%20Lab%2011%20d4452594beda4bc891d712ae3e60b83e/Untitled%203.png)

### Existing email

If we enter an existing email, we will receive a message, saying that they’ve sent a link for resetting the password to our email.

### Conclusion

This website failed the test as well. And in this case, just like with the github, we see a threat of enumeration attack, because no additional information was requested.